<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 15:16
 */

namespace App\Classes\Parser;

use App\Subcategory;
use App\Item;
use Illuminate\Support\Collection;

class SubcategoryFiller extends CategoryFiller
{

    protected $subcategory;

    public function __construct(Collection $fields, Collection $values)
    {
        parent::__construct($fields, $values);
        if(isset($this->category)) {
            $this->subcategory = Subcategory::where('name', $this->values[1])->where('parent_id', $this->category->id)->first(); // пробуем получить указанную подкатегорию
        }
    }

    /**
     * Метод подготавливает родительские таблицы.
     */
    public function prepare()
    {
        parent::prepare();
        if(!isset($this->subcategory)) { // если подкатегория не найдена
            $this->subcategory = new Subcategory();
            $this->subcategory->name = $this->values[1];
            $this->subcategory->parent_id = $this->category->id;
            $this->subcategory->save();
        }
    }

    /**
     * Метод сохраняет запись.
     */
    public function fill()
    {
        $item = new Item();
        $item->parent_id = $this->subcategory->id;
        for($i = 2; $i<count($this->values); $i++) {
            $fieldname = $this->fields[$i-2]->name;
            $item->$fieldname = $this->values[$i];
        }
        $item->save();
    }

}