<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 13:11
 */

namespace App\Classes\Parser;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

abstract class BaseCreator
{

    /**
     * Метод удаляет указанные таблицы, если они существуют.
     * @param $tables - массив имен таблиц, которые необходимо удалить.
     */
    protected function dropTables($tables) {
        foreach($tables as $table) {
            Schema::dropIfExists($table);
        }
    }

    /**
     * Метод создает таблицу items с необходимыми полями.
     * @param Collection $fields - коллекция полей которые необходимо создать для таблицы items.
     */
    protected function createItemsTable(Collection $fields) {
        try {
            Schema::create('items', function($table) use ($fields) {
                $table->increments('id');
                $table->integer('parent_id');
                foreach($fields as $field) {
                    switch($field->type) {
                        case 'double':
                            if(isset($field->size)) {
                                if(isset($field->afterDecimalPoint)) {
                                    $table->double($field->name, $field->size, $field->afterDecimalPoint);
                                } else {
                                    $table->double($field->name, $field->size, 8);
                                }
                            } else {
                                $table->double($field->name, 15, 8);
                            }
                            break;
                        case 'varchar':
                            if(isset($field->size)) {
                                $table->string($field->name, $field->size);
                            } else {
                                $table->string($field->name);
                            }
                            break;
                        case 'text':
                            $table->text($field->name);
                            break;
                        default:
                            throw new \Exception("Неподдерживаемый тип данных.");
                    }
                }
                $table->timestamps();
            });
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Метод создает таблицу categories.
     */
    protected function createCategoryTable() {
        Schema::create('categories', function($table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->timestamps();
        });
    }

}