<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 13:03
 */

namespace App\Classes\Parser;


class TableCreatorFactory
{

    /**
     * Метод исходя из указанной вложенности создаем инстанс класса создающего все необходимые для работы таблицы.
     * @param int $depthLevel - уровень сложенности.
     * @return ITableCreator - инстанс класса создающего необходимые таблицы.
     * @throws \Exception
     */
    public function create(int $depthLevel) : ITableCreator {
        switch($depthLevel) {
            case 1:
                return new CategoryCreator(); // таблицы categories и items.
            case 2:
                return new CategoryWithSubcategoryCreator(); // таблицы subcategories, categories и items.
            case 3:
                return new CategoryWithSubcategoryAndRubricCreator(); // таблицы rubrics, subcategories, categories и items.
            default:
                throw new \Exception("Неподдерживаемая вложенность категорий");
        }
    }

}