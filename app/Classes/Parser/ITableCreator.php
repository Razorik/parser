<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 12:57
 */

namespace App\Classes\Parser;

use Illuminate\Support\Collection;

interface ITableCreator
{

    public function create(Collection $fields);

}