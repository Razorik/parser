<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 15:18
 */

namespace App\Classes\Parser;


use Illuminate\Support\Collection;

class DataFillerFactory
{

    /**
     * Метод создает инстанс необходимого класса исходя из указанной вложенности.
     * @param int $depth - вложенность категорий.
     * @param Collection $fields - коллекция полей.
     * @param Collection $values - коллекция значений.
     * @return IFiller - инстанс класса для заполнения таблиц данными.
     * @throws \Exception
     */
    public function create(int $depth, Collection $fields, Collection $values) : IFiller {
        switch($depth) {
            case 1:
                return new CategoryFiller($fields, $values); // таблицы categories и items.
            case 2:
                return new SubcategoryFiller($fields, $values); // таблицы subcategories, categories и items.
            case 3:
                return new RubricFiller($fields, $values); // таблицы rubrics, subcategories, categories и items.
            default:
                throw new \Exception("Неподдерживаемый тип вложенности.");
        }
    }

}