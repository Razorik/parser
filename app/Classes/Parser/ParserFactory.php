<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 12:06
 */

namespace App\Classes\Parser;


use Illuminate\Http\Request;

class ParserFactory
{

    /**
     * Метод исходя из указанного типа парсера создает необходимый инстанс парсера.
     * @param string $type - тип парсера. Доступные типы - 'excel';
     * @param Request $request - данные запроса пользователя.
     * @return IParser - инстанс необходимого парсера.
     * @throws \Exception
     */
    public function create(string $type, Request $request) : IParser {
        switch($type) {
            case "excel":
                return new ExcelParser($request);
            default:
                throw new \Exception("Неподдерживаемый тип.");
        }
    }

}