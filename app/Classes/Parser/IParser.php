<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 09.04.2018
 * Time: 17:47
 */

namespace App\Classes\Parser;


use Illuminate\Http\Request;

interface IParser
{

    public function prepare();

    public function parse() : int;

}