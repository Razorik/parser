<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 09.04.2018
 * Time: 17:57
 */

namespace App\Classes\Parser;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ExcelParser implements IParser
{

    private $fields;
    private $file;
    private $fieldsName;
    private $depthLevel;

    public function __construct(Request $request)
    {
        $this->depthLevel = (int)$request->parser_config['parser_price_subcategory']; // получаем уровень вложенности.
        $this->fieldsName = collect(explode(';', $request->parser_config['columns'], -1)); // получаем названия полей.
        $this->file = $request->parser_config['file']; // получаем файл загруженный пользователем.
    }


    /**
     * Метод обрабатывает полученный список полей и создает необходимые таблицы.
     */
    public function prepare()
    {
        $this->fields = collect();
        foreach($this->fieldsName as $fieldName) {
            try {
                $this->fields->push($this->makeField($fieldName)); // обрабатываем очередное поле.
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }
        $factory = new TableCreatorFactory();
        try {
            $creator = $factory->create($this->depthLevel);
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
        $creator->create($this->fields);
    }

    /**
     * @return int - количество добавленных записей в таблицу.
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function parse() : int
    {
        $spreadsheet = IOFactory::load($this->file->path());
        $activeSheet = $spreadsheet->getActiveSheet();
        $row = 2; // 1 строка - названия столбцов.
        while($activeSheet->getCellByColumnAndRow(1, $row)->getValue() != null) {
            $column = 1;
            $values = collect();
            while($activeSheet->getCellByColumnAndRow($column, $row)->getValue() != null) {
                $values->push($activeSheet->getCellByColumnAndRow($column, $row)->getValue()); // сохраняем значения записи в коллекцию.
                $column++;
            }
            try {
                $factory = new DataFillerFactory();
                $filler = $factory->create($this->depthLevel, $this->fields, $values); // исходя из указанной вложенности получаем инстанс нужного класса.
                $filler->prepare(); // создаем все необходимые записи в родительских таблицах.
                $filler->fill(); // добавляем запись в таблицу.
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
            $row++;
        }
        return $row-2;
    }

    /**
     * Метод подготавливает поля к добавлению (указывает тип и размер).
     * @param string $field - имя поля.
     * @return TableField - подготовленное поле с названием, типом и размером.
     * @throws \Exception
     */
    private function makeField(string $field) : TableField {
        switch($field) {
            case 'price':
                return new TableField($field, 'double', 11, 2);
            case 'vendor_code':
                return new TableField($field, 'varchar', 255);
            case 'title':
                return new TableField($field, 'varchar', 255);
            case 'announce':
                return new TableField($field, 'text');
            case 'features':
                return new TableField($field, 'text');
            case 'description':
                return new TableField($field, 'text');
            case 'weight':
                return new TableField($field, 'double', 11, 2);
            case 'volume':
                return new TableField($field, 'double', 11, 2);
            default:
                throw new \Exception("Неподдерживаемый тип поля.");
        }
    }

}