<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 13:01
 */

namespace App\Classes\Parser;

use Illuminate\Support\Collection;

class CategoryWithSubcategoryAndRubricCreator extends CategoryWithSubcategoryCreator implements ITableCreator
{

    /**
     * Метод создает все необходимые для парсера таблицы.
     * @param Collection $fields - коллекия полей для таблицы items.
     */
    public function create(Collection $fields)
    {
        $this->dropTables(['items', 'categories', 'subcategories', 'rubrics']); // удаляем таблицы которые сейчас будем создавать.
        $this->createItemsTable($fields); // создаем таблицу items.
        $this->createCategoryTable(); // создаем таблицу categories.
        $this->createSubcategoryTable('subcategories'); // создаем таблицу subcategories.
        $this->createSubcategoryTable('rubrics'); // создаем таблицу rubrics.
    }

}