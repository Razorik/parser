<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 15:17
 */

namespace App\Classes\Parser;


interface IFiller
{

    public function prepare();

    public function fill();

}