<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 15:16
 */

namespace App\Classes\Parser;


use App\Category;
use App\Item;
use Illuminate\Support\Collection;

class CategoryFiller implements IFiller
{

    protected $fields;
    protected $values;

    protected $category;

    public function __construct(Collection $fields, Collection $values)
    {
        $this->fields = $fields;
        $this->values = $values;
        $this->category = Category::where('name', $this->values[0])->first(); // пробуем получить указанную категорию из ранее созданных
    }

    /**
     * Метод подготавливает родительские таблицы.
     */
    public function prepare()
    {
        if(!isset($this->category)) { // если категория ранее не была создана
            $this->category = new Category();
            $this->category->name = $this->values[0];
            $this->category->save(); // создаем новую, заполняем и сохраняем.
        }
    }

    /**
     * Метод сохраняет запись.
     */
    public function fill()
    {
        $item = new Item();
        $item->parent_id = $this->category->id;
        for($i = 1; $i<count($this->values); $i++) {
            $fieldname = $this->fields[$i-1]->name;
            $item->$fieldname = $this->values[$i];
        }
        $item->save();
    }

}