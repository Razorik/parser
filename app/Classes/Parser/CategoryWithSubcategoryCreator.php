<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 13:01
 */

namespace App\Classes\Parser;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

class CategoryWithSubcategoryCreator extends BaseCreator implements ITableCreator
{

    /**
     * Метод создает все необходимые для парсера таблицы.
     * @param Collection $fields - коллекия полей для таблицы items.
     */
    public function create(Collection $fields)
    {
        $this->dropTables(['items', 'categories', 'subcategories']); // удаляем таблицы которые сейчас будем создавать.
        $this->createItemsTable($fields); // создаем таблицу items.
        $this->createCategoryTable(); // создаем таблицу categories.
        $this->createSubcategoryTable('subcategories'); // создаем таблицу subcategories.
    }

    /**
     * Создает родительскую таблицу с указанными именем.
     * @param $tablename - название родительской таблицы которую необходимо создать.
     */
    protected function createSubcategoryTable(string $tablename) {
        Schema::create($tablename, function($table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('name', 255);
            $table->timestamps();
        });
    }

}