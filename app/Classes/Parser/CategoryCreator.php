<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 12:59
 */

namespace App\Classes\Parser;

use Illuminate\Support\Collection;

class CategoryCreator extends BaseCreator implements ITableCreator
{

    /**
     * Метод создает все необходимые для парсера таблицы.
     * @param Collection $fields - коллекия полей для таблицы items.
     */
    public function create(Collection $fields)
    {
        $this->dropTables(['items', 'categories']); // удаляем таблицы которые сейчас будем создавать.
        $this->createItemsTable($fields); // создаем таблицу items.
        $this->createCategoryTable(); // создаем таблицу categories.
    }

}