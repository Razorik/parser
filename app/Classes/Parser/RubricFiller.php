<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 15:17
 */

namespace App\Classes\Parser;


use App\Rubric;
use App\Item;
use Illuminate\Support\Collection;

class RubricFiller extends SubcategoryFiller
{

    protected $rubric;

    public function __construct(Collection $fields, Collection $values)
    {
        parent::__construct($fields, $values);
        if(isset($this->subcategory)) {
            $this->rubric = Rubric::where('name', $this->values[2])->where('parent_id', $this->subcategory->id)->first(); // пробуем получить указанную рубрику из ранее созданных
        }
    }

    /**
     * Метод подготавливает родительские таблицы.
     */
    public function prepare()
    {
        parent::prepare();
        if(!isset($this->rubric)) { // если рубрика не найдена
            $this->rubric = new Rubric();
            $this->rubric->name = $this->values[2];
            $this->rubric->parent_id = $this->subcategory->id;
            $this->rubric->save();
        }
    }

    /**
     * Метод сохраняет запись.
     */
    public function fill()
    {
        $item = new Item();
        $item->parent_id = $this->rubric->id;
        for($i = 3; $i<count($this->values); $i++) {
            $fieldname = $this->fields[$i-3]->name;
            $item->$fieldname = $this->values[$i];
        }
        $item->save();
    }

}