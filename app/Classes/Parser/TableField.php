<?php
/**
 * Created by PhpStorm.
 * User: ivanmelkozerov
 * Date: 10.04.2018
 * Time: 12:16
 */

namespace App\Classes\Parser;


class TableField
{

    public $name;
    public $size;
    public $type;
    public $afterDecimalPoint;

    public function __construct($name, $type, $size = null, $afterDecimalPoint = null) {
        $this->name = $name;
        $this->size = $size;
        $this->type = $type;
        $this->afterDecimalPoint = $afterDecimalPoint;
    }

}