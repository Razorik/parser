<?php

namespace App\Http\Controllers;

use App\Classes\Parser\ParserFactory;
use Illuminate\Http\Request;

class ParserController extends Controller
{

    public function parse(Request $request) {
        if($request->hasFile('parser_config.file')) { // проверяем что пользователь загрузил файл.
            $factory = new ParserFactory(); // используем фабрику для получения класса парсера, закладываемся на масштабируемость.
            try {
                $parser = $factory->create('excel', $request); // доступные варианты: 'excel'.
                $parser->prepare(); // подготавливаем таблицы для парсера.
                $itemsAdded = $parser->parse(); // парсим файл и вносим данные в таблицы
                return view('index', ['itemsAdded' => $itemsAdded]);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        } else {
            echo "Не выбран файл";
        }

    }

}
